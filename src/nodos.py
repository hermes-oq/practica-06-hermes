# Definicion de la clase 'Nodo':
class Nodo:
    def __init__(self, valor):
        # Atributo para almacenar el valor del nodo
        self.valor = valor
        # Inicialmente, el nodo siguiente es None (vacio)
        self.siguiente = None
